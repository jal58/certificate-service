# UIS Certificate Service Files

Scripts and config files related to the Certificate Service

## Generate a CSR using a script

To get your SSL/TLS certificate, you must create a Certificate Signing Request.

The easiest way to do so is using the camcsr.py script.

Here's an example:
```
python camcsr.py --ou="Institute for Example Studies" --nodes --force example.cam.ac.uk www.example.cam.ac.uk private.example.cam.ac.uk
```
The script will generate a key file and the CSR.  Install the key file on your
server and use the CSR to get your certificate.

You can check your CSR like this:


```
openssl req -text -noout -verify -in example_cam_ac_uk.csr
```

If you're going to be creating a lot of CSRs, you might want to edit the script
to hard code your institution name.

## Generate a CSR using a config file

The csr.cnf file helps you to create a CSR manually by pre-filling some of the
basic settings.  I recommend editing it to include the name of your institution
to save you typing it every time.  This is the option to choose if you want to
generate a *wildcard certificate*.

*Read the file carefully if you wish to create certificates with multiple SANs*
(i.e. that are valid for multiple FQDNs).

```
openssl req -config qv.cnf -new -nodes -out <hostname>.csr -keyout <hostname>.key
```

# Scripts for Certificate Service Internal use only

## Set up domain control verification (DCV)

This script will approve the delegation of the domain and submit it for DCV.  
Use it for non-cam.ac.uk domains.

```
dcv api-password 'Department of Studies' 'studies.example.com'
```


## Clear DCV request for domain

This script is to clear the DCV request so that you can generate a new one, for
example when the CNAME data has been lost somehow.

```
dcvclear api-password 'studies.example.com'
```
